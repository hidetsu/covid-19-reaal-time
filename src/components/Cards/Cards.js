import React from "react";
import { Card, CardContent, Typography, Grid } from "@material-ui/core";
import styles from "./Cards.module.css";
import CountUp from "react-countup";
import cx from 'classnames'

export default function Cards({
  data: { confirmed, deaths, recovered, lastUpdate }
}) {
  console.log(confirmed);
  
  if (!confirmed) {
    return "Wait...";
  }


  return (
    <div className={styles.container}>
      <Grid container spacing={3} justify="center">
        <Grid item component={Card} xs={12} md={3} className={cx(styles.cards, styles.infected)}>
          <CardContent>
            <Typography text="textSecundary" gutterBottom>
              Infected
            </Typography>
            <Typography variant="h5">
              <CountUp start="0" end={confirmed.value} separator="." />
            </Typography>
            <Typography text="textSecundary">
              {new Date(lastUpdate).toDateString()}
            </Typography>
            <Typography variant="body2">
              Number of active cases of CoronaVirus
            </Typography>
          </CardContent>
        </Grid>


        <Grid item component={Card} xs={12} md={3} className={cx(styles.cards, styles.recovered)}>
          <CardContent>
            <Typography text="textSecundary" gutterBottom>
              Recovered
            </Typography>
            <Typography variant="h5">
              <CountUp start="0" end={recovered.value} separator="." />
            </Typography>
            <Typography text="textSecundary">
              {new Date(lastUpdate).toDateString()}
            </Typography>
            <Typography variant="body2">
              Number of recoveries from CoronaVirus
            </Typography>
          </CardContent>
        </Grid>


        <Grid item component={Card} xs={12} md={3} className={cx(styles.cards, styles.deaths)}>
          <CardContent>
            <Typography text="textSecundary" gutterBottom>
              Deaths
            </Typography>
            <Typography variant="h5">
              <CountUp start="0" end={deaths.value} separator="." />
            </Typography>
            <Typography text="textSecundary">
              {new Date(lastUpdate).toDateString()}
            </Typography>
            <Typography variant="body2">
              Number of Deaths caused by CoronaVirus
            </Typography>
          </CardContent>
        </Grid>
      </Grid>
    </div>
  );
}
